########################################
##  missionaries_n_cannibals_problem  ##
##       breadth-first search         ##
########################################

class stru:
    def __init__(state):
        # property
        state.cannibals = 0 #  number of cannibals on the right side
        state.missionaries = 0 #  number of missionaries on the right side
        state.boat = 0 #  boat is on the right side
        # other
        state.depth = 0
        state.next = []
        state.parent = None
        state.action = None

def check_missionaries_die( state ):
# function return false if cannibals can eat missionaries (check from input_state)  
# false state (cannibals can eat missionaries went input state is)
    # <0,1,?>
    # <3,2,?>
    # <0,2,?>
    # <3,1,?>
    # <2,1,?>
    # <1,2,?>
    if state[0] == 0 and state[1] == 1 :
        return False
    elif state[0] == 3 and state[1] == 2 :
        return False
    elif state[0] == 0 and state[1] == 2 :
        return False
    elif state[0] == 3 and state[1] == 1 :
        return False
    elif state[0] == 2 and state[1] == 1 :
        return False
    elif state[0] == 1 and state[1] == 2 :
        return False
    else:
        return True

def action_result(node, action_number):
    # from node state and input action_number this function will return new state with new state
    # ( # of cannibal , # of missionaries , boat is on rigth side)
    # and return false if return state is impossible or missionaries die
    new_result = [node.cannibals, node.missionaries, node.boat]
    if action_number == 1:
        if node.boat == 1:
            new_result[0] = new_result[0] - 1
            new_result[2] = 0
        else :
            new_result[0] = new_result[0] + 1
            new_result[2] = 1

    elif action_number == 2:
        if node.boat == 1:
            new_result[0] = new_result[0] - 2
            new_result[2] = 0
        else:
            new_result[0] = new_result[0] + 2
            new_result[2] = 1

    elif action_number == 3:
        if node.boat == 1:
            new_result[1] = new_result[1] - 1
            new_result[2] = 0
        else:
            new_result[1] = new_result[1] + 1
            new_result[2] = 1

    elif action_number == 4:
        if node.boat == 1:
            new_result[1] = new_result[1] - 2
            new_result[2] = 0
        else:
            new_result[1] = new_result[1] + 2
            new_result[2] = 1

    else :
        if node.boat == 1:
            new_result[0] = new_result[0] - 1
            new_result[1] = new_result[1] - 1
            new_result[2] = 0
        else:
            new_result[0] = new_result[0] + 1
            new_result[1] = new_result[1] + 1
            new_result[2] = 1

    if(new_result[0] >= 0 and new_result[0] <= 3 and new_result[1] >= 0 and new_result[1] <= 3 and check_missionaries_die(new_result)):
        return new_result
    else:
        return False

def try_action(node): #1-5
    # try give action to input node ( try to expand state node)
    # loop try action from action 1 to 5
    # if any action possible then create new state node 
    if(len(node.next) > 0 ):
        return
    else:        
        for action in  range(1, 6):
            result_action = action_result(node, action)
            if result_action:
                new_node = stru()
                new_node.cannibals = result_action[0]
                new_node.missionaries = result_action[1]
                new_node.boat = result_action[2]
                new_node.next = []
                new_node.parent = node
                new_node.depth = node.depth + 1
                new_node.action = action
                
                node.next.append(new_node)

def is_goal_state(node):
    # function check input node is goal state ?
    # if input node is goal state then return True
    # else return False
    if node != None :
        if node.cannibals == 3 and node.missionaries == 3 and node.boat == 1:
            return True
        else:
            return False

def enqueue( node, queue  ):
    for next_element in node.next:
        queue.append(next_element)

def breadth_first_search( queue ):
    if len(queue) > 0 : 
        first_queue = queue[0]
        queue.remove(first_queue)
        while not is_goal_state(first_queue):
            # print("<", first_queue.cannibals, first_queue.missionaries, first_queue.boat, '> - depth : ', first_queue.depth, ">")
            try_action(first_queue)
            enqueue(first_queue, queue)
            # dequeue
            if( len(queue) > 0 ):
                first_queue = queue[0]
                queue.remove(first_queue)
            else:
                print("queue out")
                return
        return first_queue
    else:
        print("notthing in queue")

def print_to_parent(node):
    if( node ):
        print_to_parent(node.parent)
        print("<", node.cannibals, node.missionaries, node.boat, '> - depth : ', node.depth,' action - ', node.action)

# init state
init_state = stru()
init_state.cannibals = 0
init_state.missionaries = 0
init_state.boat = 0
init_state.depth = 0

queue = []

queue.append(init_state)
succeed = breadth_first_search(queue)
if( succeed and is_goal_state(succeed)):
    print_to_parent(succeed)
    