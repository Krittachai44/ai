########################################
##  missionaries_n_cannibals_problem  ##
##    iterative-deepening search      ##
########################################


# defined init state and goal state
# defined max_limit
# loop from limit 0 to max_limit for Depth-limited search

# defined all action 

# defined function
import time
start_time = time.time()
succeed = False
WriteToFile = []
NEWLINE = '\n'

class stru:
    def __init__(state):
        # property
        state.cannibals = 0 #  number of cannibals on the right side
        state.missionaries = 0 #  number of missionaries on the right side
        state.boat = 0 #  boat is on the right side
        # other
        state.depth = 0
        state.next = []
        state.parent = None
        state.action = None

# def print_tree( node ):
#     if node == None :
#         return
#     else :
#         for nxt in node.next :
#             print_tree( nxt )
#         print()
        

def create_state():
    print("create_state")


def check_missionaries_die( state ):
# function return false if cannibals can eat missionaries (check from input_state)  
# false state (cannibals can eat missionaries went input state is)
    # <0,1,?>
    # <3,2,?>
    # <0,2,?>
    # <3,1,?>
    # <2,1,?>
    # <1,2,?>
    if state[0] == 0 and state[1] == 1 :
        return False
    elif state[0] == 3 and state[1] == 2 :
        return False
    elif state[0] == 0 and state[1] == 2 :
        return False
    elif state[0] == 3 and state[1] == 1 :
        return False
    elif state[0] == 2 and state[1] == 1 :
        return False
    elif state[0] == 1 and state[1] == 2 :
        return False
    else:
        return True

def action_result(node, action_number):
    # from node state and input action_number this function will return new state with new state
    # ( # of cannibal , # of missionaries , boat is on rigth side)
    # and return false if return state is impossible or missionaries die
    new_result = [node.cannibals, node.missionaries, node.boat]
    if action_number == 1:
        if node.boat == 1:
            new_result[0] = new_result[0] - 1
            new_result[2] = 0
        else :
            new_result[0] = new_result[0] + 1
            new_result[2] = 1

    elif action_number == 2:
        if node.boat == 1:
            new_result[0] = new_result[0] - 2
            new_result[2] = 0
        else:
            new_result[0] = new_result[0] + 2
            new_result[2] = 1

    elif action_number == 3:
        if node.boat == 1:
            new_result[1] = new_result[1] - 1
            new_result[2] = 0
        else:
            new_result[1] = new_result[1] + 1
            new_result[2] = 1

    elif action_number == 4:
        if node.boat == 1:
            new_result[1] = new_result[1] - 2
            new_result[2] = 0
        else:
            new_result[1] = new_result[1] + 2
            new_result[2] = 1

    else :
        if node.boat == 1:
            new_result[0] = new_result[0] - 1
            new_result[1] = new_result[1] - 1
            new_result[2] = 0
        else:
            new_result[0] = new_result[0] + 1
            new_result[1] = new_result[1] + 1
            new_result[2] = 1

    if(new_result[0] >= 0 and new_result[0] <= 3 and new_result[1] >= 0 and new_result[1] <= 3 and check_missionaries_die(new_result)):
        return new_result
    else:
        return False

def try_action(node): #1-5
    # try give action to input node ( try to expand state node)
    # loop try action from action 1 to 5
    # if any action possible then create new state node 
    if(len(node.next) > 0 ):
        return
    else:        
        for action in  range(1, 6):
            result_action = action_result(node, action)
            if result_action:
                new_node = stru()
                new_node.cannibals = result_action[0]
                new_node.missionaries = result_action[1]
                new_node.boat = result_action[2]
                new_node.next = []
                new_node.parent = node
                new_node.depth = node.depth + 1
                new_node.action = action
                
                node.next.append(new_node)

def is_goal_state(node):
    # function check input node is goal state ?
    # if input node is goal state then return True
    # else return False
    if node != None :
        if node.cannibals == 3 and node.missionaries == 3 and node.boat == 1:
            return True
        else:
            return False

def depth_limited_search(node, limit):
    # function recursive depth first search from root state to state that depth less than input limit 
    if( node.depth <= limit ):
        # print("try_action", node)
        try_action(node)
        for x in node.next:
            test_node = depth_limited_search(x , limit)
            if is_goal_state(test_node):
                print("found"+"hi")
                return test_node
            
        return node
            # return succeed

    else:
        return None


def print_tree(node, limit):
    # function print state tree (maybe hard to see)
    # recursive
    if(node.depth <= limit):
        for x in node.next:
            print_tree(x, limit)
        if( node.depth > 0 ):
            print("<", node.cannibals, node.missionaries, node.boat, '> - depth : ', node.depth, ' from ', node.parent.cannibals,node.parent.missionaries,node.parent.boat, ' action - ', node.action) 
        else :
            print("<", node.cannibals, node.missionaries, node.boat, '> - depth : ', node.depth,' from ', node.parent)
    else:
        return None

def print_to_parent(node):
    if( node ):
        print_to_parent(node.parent)
        print("<", node.cannibals, node.missionaries, node.boat, '> - depth : ', node.depth,' action - ', node.action)
        WriteToFile.append("<")
        WriteToFile.append(node.cannibals)
        WriteToFile.append(",")
        WriteToFile.append(node.missionaries)
        WriteToFile.append(",")
        WriteToFile.append(node.boat)
        WriteToFile.append("> depth : ")
        WriteToFile.append(node.depth)
        WriteToFile.append(" action : ")
        WriteToFile.append(node.action)
        WriteToFile.append(NEWLINE)


# defined
MAX_LIMIT = 11

# init state
init_state = stru()
init_state.cannibals = 0
init_state.missionaries = 0
init_state.boat = 0
init_state.depth = 0

depth = 0
# check if init_state is goal state
if( succeed and is_goal_state(succeed) ):
    time = time.time() - start_time
    print("found !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ")
    print("<", succeed.cannibals, succeed.missionaries, succeed.boat, '> - depth : ', succeed.depth)
    
    print_to_parent(succeed)
    
    print("--- %s seconds ---" % (time))
else:
    while( depth <= MAX_LIMIT ):
        # clear_tree(init_state)
        # print("=============== clear ==============", depth)
        succeed = depth_limited_search(init_state, depth)
        # print("++++++++++++++++++++++++++++++++++++++")
        # print_tree(init_state,depth)
        if( succeed and is_goal_state(succeed) ):
            time = time.time() - start_time
            print("found !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ")
            print("<", succeed.cannibals, succeed.missionaries, succeed.boat, '> - depth : ', succeed.depth)
            
            print_to_parent(succeed)
            
            print("--- %s seconds ---" % (time))
            break
        depth = depth + 1


totalTime = (time)
WriteToFile.append("TIME : ")
WriteToFile.append(totalTime)
list1 = WriteToFile
str1 = ''.join(str(e) for e in list1)
text_file = open("Output.txt", "w")
text_file.write(str1)
text_file.close()