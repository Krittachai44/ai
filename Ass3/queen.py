from random import randint
import math
import random

def find_prop(deltaE,T):
  return 1/pow((math.e),deltaE/T)

def new_move(max,table,wrong_q_table):
  for col in range(0, max):
    for row in range(0, max):
      if table[row][col]:
        if wrong_q_table[row][col] :
          if random.random() < 0.5:
            table[row][col] = False
            new_queen_row = randint(0,max-1)
            table[new_queen_row][col] = True
        else :
          if random.random() < 0.3:
            table[row][col] = False
            new_queen_row = randint(0,max-1)
            table[new_queen_row][col] = True
  return table