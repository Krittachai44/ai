from turtle import *
import turtle
import tkinter
import math

square_size = 30

tur = turtle.Turtle()
tur.speed(10)
tur.hideturtle() # hide cursor of turtle

screen = turtle.Screen()
screen.setup(width=800, height=600, startx=0, starty=0) #set screen size
screen.bgcolor("#355c7d")
screen.title("N-queen problem")
screen.tracer(False)
# screen.addshape(image)
# tur.shape(image)

board_size = screen.numinput("N-queen", "Number of queen:", 1, minval=1, maxval=10) #input for N-queen
# print(board_size)

def queen(pos_x, pos_y):
    tmp_x = -(square_size*board_size)/2
    tmp_y = -(square_size*board_size)/2
    newpos_x = (pos_x + 1) * tmp_x
    newpos_y = -(pos_y + 1) * tmp_y
    if(pos_y > 0):
        newpos_y = tmp_y
    
    print(tmp_y)
    print(pos_y)
    tur.color('#858585')
    tur.begin_fill()
    tur.penup()
    tur.up()
    tur.setpos(newpos_x+8, newpos_y+5)
    tur.pendown()
    tur.goto(newpos_x+4, newpos_y+22)
    tur.goto(newpos_x+11, newpos_y+15)
    tur.goto(newpos_x+15, newpos_y+26)
    tur.goto(newpos_x+19, newpos_y+15)
    tur.goto(newpos_x+26, newpos_y+22)    
    tur.goto(newpos_x+22, newpos_y+5)
    tur.goto(newpos_x+8, newpos_y+5)
    tur.end_fill()


def square(size_in, color_in):
    # print(color_in)
    tur.color(color_in)
    tur.begin_fill()
    for i in range(4):
        tur.forward(square_size) #forward for 30px
        tur.left(90) #turn left arrow for 90 degree
    tur.end_fill()
    tur.forward(square_size)


def eachRow(half, color_1, color_2, n_black, n_white, swap):
    for j in range(half):
        if swap:
            square(n_black,color_1)
            square(n_white,color_2)
        else:
            square(n_white,color_2)
            square(n_black,color_1)

def table(board_size, swap, color_1, color_2):
    tur.up() #turn arrow up
    tur.setpos(-(square_size*board_size)/2, (square_size*board_size)/2)
    half = math.ceil(board_size/2)
    if(board_size%2 == 0):
        n_black = half
        n_white = n_black
    else:
        n_black = half
        n_white = n_black - 1 
    
    for i in range(board_size):
        if(board_size%2 == 0):
            n_black = half
            n_white = n_black
            eachRow(half, color_1, color_2, n_black, n_white, swap)
        else:
            n_black = half
            n_white = n_black - 1
            eachRow(half-1, color_1, color_2, n_black, n_white ,swap)
            if swap:                    
                square(n_black,color_1)
            else:
                square(n_white,color_2)
        
        tur.backward(square_size*board_size) # back to left
        tur.right(90)
        tur.forward(square_size)
        tur.left(90)
        if swap:
            swap = False
        else :
            swap = True

table(int(board_size), True, 'black', 'white')

# queen(0,0)
queen(0,1)
queen(0,2)
# queen(1,0)
# queen(1,1)
# queen(1,2)
# queen(2,0)
# queen(2,1)
# queen(2,2)
done()
