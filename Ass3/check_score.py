def ret_score(n,table,wrong_q_table):
  max_score = n*(n-1)/2
  current_score = max_score
  for row in range(0, n):
    for col in range(0, n):
      wrong_q_table[row][col] = False
  # print current_score
  for row in range(0,n):
    for col in range(0,n):
      if table[row][col] : # found queen
        # check with next queen
        first = True
        for new_row in range( row , n ):
          for new_col in range( 0 , n):
            if first:
              first = False
              new_col = col
            if table[new_row][new_col]: # found next queen
              if row == new_row and col == new_col:
                continue
              if row == new_row: # same row
                current_score = current_score - 1
                wrong_q_table[row][col] = True
                wrong_q_table[new_row][new_col] = True
              elif new_row - row == col - new_col:
                current_score = current_score - 1
                wrong_q_table[row][col] = True
                wrong_q_table[new_row][new_col] = True
              elif col == new_col:
                current_score = current_score - 1
                wrong_q_table[row][col] = True
                wrong_q_table[new_row][new_col] = True
              elif new_row - row == new_col - col:
                current_score = current_score - 1
                wrong_q_table[row][col] = True
                wrong_q_table[new_row][new_col] = True

  
  return current_score


# max_table = 4
# table = [[False for x in range(max_table)] for y in range(max_table)] 

# table[1][0] = True
# table[3][1] = True
# table[0][2] = True
# table[2][3] = True
# # table[4][4] = True
# # table[4][5] = True

# print ret_score(max_table , table)     