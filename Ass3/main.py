import check_score
import queen
import random
import sys
# from drawQueen import gui_queen
import reInput
import assign


# debug var
plot_value = []
# debug var

def main():
  import time
  
  ###############################################################
  #                          RED ZONE                           # 
  ###############################################################
  import reInput
  ###############################################################
  #                         END RED ZONE                        # 
  ###############################################################
  gui = True
  temp = 10000000
  cooldown_temp = 1
  
  print('call input')
  max_table = reInput.Init()
  if( max_table == 99 or max_table == 0):
    exit()
  else:
    # max_table = int(reInput.sent_Input())
    print('max: ',max_table)
    start_time = time.time()

    table = [[False for x in range(max_table)] for y in range(max_table)]
    wrong_q_table = [[False for x in range(max_table)] for y in range(max_table)]

    max_score = max_table*(max_table-1)/2


    for row in range(0, max_table):
      for col in range(0, max_table):
        if row == col :
          table[row][col] = True

    if gui:
      itemQ = []
      tk = assign.Tk()
      canvas = assign.createMotion(table,itemQ,tk) #temp is list
      print ("first table")
      
    while temp > 0:
      temp_table = queen.new_move(max_table,table,wrong_q_table)
      old_score = check_score.ret_score(max_table,table,wrong_q_table)
      plot_value.append(old_score)
      new_score = check_score.ret_score(max_table,temp_table,wrong_q_table)
      if temp % 2500 == 0:
        if gui:
          assign.changeItem(tk, canvas,itemQ, table, False)
        print ( temp, new_score,end='\r')
      if new_score == max_score:
        table = temp_table
        time = time.time() - start_time
        print ("found it")
        break
      if new_score > old_score:
        # move
        table = temp_table
      else:
        delta_e = old_score - new_score
        prop = queen.find_prop(delta_e, temp)
        if random.random() < prop:
          table = temp_table
      
      temp = temp - cooldown_temp
      # print temp
    for row in range(0, max_table):
      for col in range(0, max_table):
        if table[row][col]:
          print (row,col,table[row][col])
    if gui:
      assign.changeItem(tk, canvas,itemQ, table, True)
    print (max_score,check_score.ret_score(max_table,table,wrong_q_table))
    print (temp)
    print (time)
    # if gui:
    #   table = reInput.sent_Input()
    #   print ('input: ', table)
    # print plot_value


while True :
  main()