from tkinter import *
import time

def initItem(root, canvas, board):
    queen = PhotoImage(file="queen.png")
    queen = queen.subsample(5, 5)
    root.queen = queen
    length = len(board)
    for y in range(length):
        for x in range(length):
            if(board[y][x]):
                itemQ.append(canvas.create_image(((70*x)+10, (70*y)+10), image=queen, anchor='nw'))
    return itemQ

def changeItem(root, canvas, board):
    length = len(board)
    yspeed = 0
    for y in range(length):
        for x in range(length):
            if(board[y][x]):
                pos = canvas.coords(itemQ[y])
                if pos[0] <= (70*x)+10:
                    xspeed = 5
                else:
                    xspeed = -5
                while True:
                    canvas.move(itemQ[y], xspeed, yspeed)
                    pos = canvas.coords(itemQ[y])
                    if xspeed == 5 and pos[0] >= ((70*(x))+10):
                        break
                    if xspeed == -5 and pos[0] <= ((70*x)+10):
                        break
                    root.update()
                    time.sleep(0.05)

    return

def createMap(board,root):
    length = len(board)

    startframe = Frame(root)
    canvas = Canvas(startframe, width=70*length, height=70*length)

    startframe.pack()
    canvas.pack()

    one = PhotoImage(file="chess_1.png")
    two = PhotoImage(file="chess_2.png")
    one = one.subsample(2, 2)
    two = two.subsample(2, 2)
    root.one = one  # to prevent the image garbage collected.
    root.two = two
    for y in range(length):
        for x in range(length):
            if x % 2 == y % 2:
                canvas.create_image((70 * x, 70*y), image=one, anchor='nw')
            else:
                canvas.create_image((70 * x, 70*y), image=two, anchor='nw')
    return canvas

def createMotion(board,root):
    canvas = createMap(board,root)
    initItem(root, canvas, board)
    return canvas

tk = Tk()
itemQ = []
# canvas = createMotion(temp,tk) //temp is list
# changeItem(tk, canvas, temp)
mainloop()
