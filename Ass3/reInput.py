from tkinter import *

def Init():
    def sent_Input():
        print("sent", var.get())
        return var.get()
    def clickBtn(val):
        print ("Click on", val)
        var.set(val)
        print ("new Input :", var.get())
        sent_Input()
        tkInput.destroy()

    def buildInput(root):

        startframe = Frame(root)
        canvas = Canvas(startframe, width=200, height=340, bg='#e1e0e0') 
        w = Message(root, bg='#e1e0e0', text="Select number of n-Queen")
        w.pack()
        w.place(height=50,width=200,x=0,y=10)

        startframe.pack()
        canvas.pack()

        btn = {}
        for j in range(4):
            for i in range(3):
                if (i+1 + (3*j)) > 10:
                    break
                action = lambda x = (i+1 + (3*j)): clickBtn(x)
                btn[(i+1 + (3*j))] = Button(root, borderwidth=0, activebackground='#c7c2b3',bg='#c1bcab', text=(i+1 + (3*j)), command=action)
                btn[(i+1 + (3*j))].pack()
                btn[(i+1 + (3*j))].config(font=("Courier", 20))
                btn[(i+1 + (3*j))].place(height=70, width=70 ,
                            x=70*i, y=(70*j)+70)
        actionExit = lambda x = 99: clickBtn(x)
        exitBtn = Button(root, borderwidth=0, activebackground='#e29657', bg='#db7c2e', text="Exit", command=actionExit)
        exitBtn.pack()
        exitBtn.config(font=("Courier", 16))
        exitBtn.place(bordermode=OUTSIDE, height=70, width=140 ,x=70, y=280)
        return canvas


    tkInput = Tk()
    var = IntVar()
    canvas = buildInput(tkInput)
    tkInput.mainloop()
    return var.get()
