from tkinter import *
import time

def initItem(root,itemQ, canvas, board):
    queen = PhotoImage(file="queen_02.png")
    queen = queen.subsample(5, 5)
    root.queen = queen
    length = len(board)
    for y in range(length):
        for x in range(length):
            if(board[y][x]):
                itemQ.append(canvas.create_image(((70*x)+10, (70*y)+10), image=queen, anchor='nw'))
    return itemQ

def changeItem(root, canvas,itemQ, board,loop):
    length = len(board)
    yspeed = 0
    for y in range(length):
        for x in range(length):
            if(board[y][x]):
                pos = canvas.coords(itemQ[y])
                if pos[0] <= (70*x):
                    xspeed = 10
                else:
                    xspeed = -10
                while True:
                    canvas.move(itemQ[y], xspeed, yspeed)
                    pos = canvas.coords(itemQ[y])
                    if xspeed == 10 and pos[0] >= ((70*(x))+10):
                        break
                    if xspeed == -10 and pos[0] <= ((70*x)+10):
                        break
                    root.update()
    if loop :
        wid = 70*length
        if wid < 140:
            wid = 140
        w = Message(root, bg='#f0e1cc', text="DONE.")
        w.pack()
        w.place(height=50,width=100,x=wid/2 - 50,y=(70*length)+10)

        btn = Button(root, text="Enter New Input" ,activebackground='#b9988a', bg='#9d7c67', borderwidth=0,command=lambda:root.destroy())
        btn.place(width=100, height= 50,x=wid/2 - 50,y=(70*length)+80)
        mainloop()       

    return

def createMap(board,root):
    length = len(board)

    startframe = Frame(root)
    wid = 70*length
    if wid < 140:
        wid = 140

    canvas = Canvas(startframe, width=wid, height=70*length + 160, bg='#f0e1cc')

    startframe.pack()
    canvas.pack()

    one = PhotoImage(file="chess_1.png")
    two = PhotoImage(file="chess_2.png")
    one = one.subsample(2, 2)
    two = two.subsample(2, 2)
    root.one = one  # to prevent the image garbage collected.
    root.two = two
    for y in range(length):
        for x in range(length):
            if x % 2 == y % 2:
                canvas.create_image((70 * x, 70*y), image=one, anchor='nw')
            else:
                canvas.create_image((70 * x, 70*y), image=two, anchor='nw')
    return canvas

def createMotion(board,itemQ,root):
    canvas = createMap(board,root)
    initItem(root,itemQ, canvas, board)
    return canvas
# temp = [[1,0,0],[0, 1,0],[0,0,1]]
# tk = Tk()
# itemQ = []
# canvas = createMotion(temp,tk) 
# changeItem(tk, canvas, temp)
# mainloop()
