from graphics import *

win = GraphWin('Romania Map', 750, 470) # give title and dimensions
win.yUp()

a = [50, 350]
b = [460, 140]
c = [260, 80]
d = [150, 100]
e = [700, 80]
f = [320, 320]
g = [430, 60]
h = [660, 170]
i = [580, 370]
l = [150, 200]
m = [155, 150]
n = [480, 410]
o = [100, 450]
p = [340, 190]
r = [240, 260]
s = [200, 325]
t = [60, 250]
u = [540, 170]
v = [630, 310]
z = [70, 400]

town = [a,b,c,d,e,f,g,h,i,l,m,n,o,p,r,s,t,u,v,z]
townName = ['Arad', 'Bucharest', 'Craiova', 'Drobeta', 'Eforie', 'Fagaras', 'Giurgiu', 'Hirsova', 'Iasi', 'Lugoj', 'Mehadia', 'Neamt', 'Oradea', 'Pitesti', 'Rimnicu Vilcea', 'Sibiu', 'Timisoara', 'Urziceni', 'Vaslui', 'Zerind']

def checkVar(d_in):
    d_in = ord(d_in)
    num = d_in - 97
    temp = num
    if(num >= 11):#skip jk -2
        temp = num - 2
    if(num >= 16):#skip q -1
        temp = temp - 1
    if(num == 25):#skip wxy -3
        temp = temp - 3
    num = temp

    for i in range(0,len(townName)):
        if(i == num):
            temp = town[i]
            x = temp[0]
            y = temp[1]
            #A
            if(i == 0):
                x = x - 40
                y = y - 10
            #B
            if(i == 1):
                x = x + 20
                y = y - 30                
            #c
            if(i == 2):
                y = y - 30
            #D
            if(i == 3):
                x = x - 60
                y = y - 10
            #G
            if(i == 6):
                x = x - 50
                y = y - 10
            #M
            if(i == 10):
                x = x - 60
                y = y - 10
            #R
            if(i == 14):
                x = x + 50
                y = y - 10
            #T
            if(i == 16):
                x = x + 30
                y = y - 10
            #U
            if(i == 17):
                x = x - 35
            #Z
            if(i == 19):
                x = x + 20
                y = y - 20
            
            message = Text(Point(x+10 , y+10), townName[i])
            message.draw(win)
            
    for i in range(0,len(town)):
        if(i == num):
            return town[i]

def square(position, color):
    
    bottomLeft = Point(position[0]-10, position[1]-10)
    position = Point(position[0], position[1])
    rect = Rectangle(bottomLeft, position)
    rect.setFill(color)
    rect.setWidth(4)
    rect.draw(win)


def drawPath(init, end, color, width):
    newInit = checkVar(init)
    newEnd = checkVar(end)

    start = Point(int(newInit[0])-5, int(newInit[1])-5)
    stop = Point(int(newEnd[0])-5, int(newEnd[1])-5)
    path = Line(start, stop)
    path.setWidth(width)
    path.setOutline(color)
    path.draw(win)

def close():
    win.getMouse()
    clean()
    # win.close()

def clean():
    drawPath('o', 'z', 'white', 5)
    drawPath('o','s','white',5)
    drawPath('z','a','white',5)
    drawPath('a','t','white',5)
    drawPath('t','l','white',5)
    drawPath('l','m','white',5)
    drawPath('m','d','white',5)
    drawPath('a','s','white',5)
    drawPath('d','c','white',5)
    drawPath('r','c','white',5)
    drawPath('s','r','white',5)
    drawPath('s','f','white',5)
    drawPath('r','p','white',5)
    drawPath('f','b','white',5)
    drawPath('b','g','white',5)
    drawPath('b','u','white',5)
    drawPath('u','h','white',5)
    drawPath('h','e','white',5)
    drawPath('u','v','white',5)
    drawPath('i','v','white',5)
    drawPath('i','n','white',5)
    drawPath('p','c','white',5)
    drawPath('p','b','white',5)

    drawTown()

def drawTown():
    
    drawPath('o', 'z', 'black', 2)
    drawPath('o','s','black',2)
    drawPath('z','a','black',2)
    drawPath('a','t','black',2)
    drawPath('t','l','black',2)
    drawPath('l','m','black',2)
    drawPath('m','d','black',2)
    drawPath('a','s','black',2)
    drawPath('d','c','black',2)
    drawPath('r','c','black',2)
    drawPath('s','r','black',2)
    drawPath('s','f','black',2)
    drawPath('r','p','black',2)
    drawPath('f','b','black',2)
    drawPath('b','g','black',2)
    drawPath('b','u','black',2)
    drawPath('u','h','black',2)
    drawPath('h','e','black',2)
    drawPath('u','v','black',2)
    drawPath('i','v','black',2)
    drawPath('i','n','black',2)
    drawPath('p','c','black',2)
    drawPath('p','b','black',2)

    for i in range(0, len(town)):
        square(town[i], 'white')
    

    # win.getMouse()
    # win.close()
