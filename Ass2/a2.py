# city =  [['a','z','s','t'],
#          ['b','f','g','p','u'],
#          ['c','d','r','p'],
#          ['d','c','m'],
#          ['e','h'],
#          ['f','b','s'],
#          ['g','b'],
#          ['h','e','u'],
#          ['i','n','v'],
#          ['l','m','t'],
#          ['m','d','l'],
#          ['n','i'],
#          ['o','s','z'],
#          ['p','b','c','r'],
#          ['r','c','p','s'],
#          ['s','a','f','o'],
#          ['t','a','l'],
#          ['u','b','h','v'],
#          ['v','l','u'],
#          ['z','a','o']]

# print(city)

import csv
import codecs
import time
from drawMap import test_graphic

codecs.BOM_UTF8
'\xef\xbb\xbf'

def f(x) :
    switcher = {
        'a': 1,
        'b': 2,
        'c': 3,
        'd': 4,
        'e': 5,
        'f': 6,
        'g': 7,
        'h': 8,
        'i': 9,
        'l': 10,
        'm': 11,
        'n': 12,
        'o': 13,
        'p': 14,
        'r': 15,
        's': 16,
        't': 17,
        'u': 18,
        'v': 19,
        'z': 20,
    }
    return switcher.get(x, "incorrect city")

def selectCity(firstCity,secondCity):
    y = 0
    x = 0
    with open('AI3.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter = ',')
        data = []
        for row in readCSV :
            # insert data to array 2 D
            data.append(row)
        # print (data[0][5])
        # print (data[1:21])

    x = f(firstCity)
    while True:
        if x == 'incorrect city':
            print(x)
            firstCity = input("Your location : ")
            x = f(firstCity)
        else:
            break

    y = f(secondCity)
    while True:
        if y == 'incorrect city':
            print(y)
            secondCity = input("Your destination : ")
            y = f(secondCity)
        else:
            # print (data[x][y])
            return int(data[x][y])
            break

###############################################
###############################################

class city:
    def __init__(state):
        state.name = None
        state.next = []
        state.next_dis = []
        # def printname(self):
        #     print(self.name)

class str_node:
    def __init__(state):
        state.name = ''
        state.next = []
        state.pre = None
        state.dist = 0  
        state.displacement = 0

city_ary = []       # city array contain all city
queue = []

# print town detail ( name and connected town)
def print_all_city(citys):
    for city in citys:
        print('town : ',city.name,' next is ',city.next)

# def best_first_search(  , cost )

def find_dist( src , des):
    for current_city in city_ary:
        if src.name == current_city.name:
            for index in range(0,len(current_city.next)):
                if current_city.next[index] == des :
                    return current_city.next_dis[index]
    return 0

# split node
def split_node( input_node, final ): # final is char
    for current_city in city_ary:
        if input_node.name == current_city.name:
            for index in range(0,len(current_city.next)):
                new_node = str_node()
                new_node.name = current_city.next[index]
                new_node.dist = input_node.dist + current_city.next_dis[index]
                new_node.displacement = selectCity(new_node.name,final) 
                # print ('new node ', new_node.name ,'have ',new_node.dist,new_node.displacement,new_node.dist+new_node.displacement)
                input_node.next.append(new_node)
                new_node.pre = input_node

                queue.append(new_node)
                # if new_node.name == final:
                #     return new_node
    return None

def ret_min_val():
    # for x in queue:
    #     print 'in ret_min_val queue have ',x.name
    min_is = 0
    min = queue[0].dist + queue[0].displacement
    # print 'console loggggg : ',queue[0].name,queue[0].dist,queue[0].displacement,min,min_is
    for index in range(0,len(queue)):
        # print 'now is ',queue[index].name
        if min > queue[index].dist + queue[index].displacement:
            min = queue[index].dist + queue[index].displacement
            # print 'console loggggg in loop : ',queue[index].name,queue[index].dist,queue[index].displacement,min,min_is
            min_is = index
    ret_node = queue[min_is]
    queue.remove(ret_node)
    # print ('ret_node is : ',ret_node.name)
    return ret_node

# A start search
def a_star_search(node,goal): # goal is char
    # best first search
    # queueing function
    # print ('##################################################')
    current_node = ret_min_val()
    if current_node.name == goal:
        return current_node
    return split_node(current_node,goal)
    # first_queue = queue[0]
    # queue.remove(first_queue)
    

def is_goal(node,goal):
    if node.name == goal :
        return True
    else :
        return False

def print_to_parent(node):
    if( node ):
        print_to_parent(node.pre)
        print (node.name)
        if node.pre :
            test_graphic.drawPath(node.name,node.pre.name,'red',5)
            time.sleep(0.5)
            

# tell which town connect
with codecs.open('map2.csv','r',encoding="utf-8-sig") as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    index = 0
    for row in readCSV:
        city_ary.append(city())
        # city_ary[index] = city()
        first = True
        dist = False
        for town in row:
            if not town == '':
                if first:
                    first = False
                    city_ary[index].name = town
                    continue
                if not dist :
                    city_ary[index].next.append(town)
                    dist = True
                else :
                    city_ary[index].next_dis.append(int(town))
                    dist = False

        # print('town : ',city_ary[index].name,' next is ',city_ary[index].next)
        # print('town : ',city_ary[index].name,' next is ',city_ary[index].next_dis)
        index = index + 1

# #  check city detail
# print_all_city(city_ary)

# while 1:
    test_graphic.drawTown()
    print('to exit please press 0')
    init_city = input(' Enter start city : ')
    # if init_city == 0:
    #     break
    if f(init_city) == 'incorrect city':
        city_input = False   
        while not city_input:
            init_city = input(' Enter start city : ')
            init_state = str_node()
            res = f(init_city)
            if not res == 'incorrect city':
                break


    city_input = False
    while not city_input:
        final_city = input(' Enter final city : ')      
        init_state = str_node()
        res = f(final_city)
        if not res == 'incorrect city':
            break
                        
    print (init_city, final_city)
    init_state.name = init_city


    if is_goal(init_state,final_city):
        print ('found!!')
    else :
        queue.append(init_state)
        for loop in range(0,10000):
            node = a_star_search(init_state,final_city)
            # if node and is_goal(node,final_city):
            if node:
                test_graphic.drawTown()
                print ('found!!! in ',loop)
                time.sleep(0.5)
                print_to_parent(node)
                break


    test_graphic.close()
