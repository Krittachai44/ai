from graphics import *

win = GraphWin('Romania Map', 750, 470) # give title and dimensions
win.yUp()

a = [50, 350]
b = [460, 140]
c = [260, 80]
d = [150, 100]
e = [700, 80]
f = [320, 320]
g = [430, 60]
h = [660, 170]
i = [580, 370]
l = [150, 200]
m = [155, 150]
n = [480, 410]
o = [100, 450]
p = [340, 190]
r = [240, 260]
s = [200, 325]
t = [60, 250]
u = [540, 170]
v = [630, 310]
z = [70, 400]

town = [a,b,c,d,e,f,g,h,i,l,m,n,o,p,r,s,t,u,v,z]

def checkVar(d_in):
    d_in = ord(d_in)
    num = d_in - 97
    temp = num
    if(num >= 11):
        temp = num - 2
    if(num >= 16):
        temp = temp - 1
    if(num == 25):
        temp = temp - 3
    print(temp)
    num = temp

    for i in range(0,len(town)):
        if(i == num):
            return town[i]

def square(x_in, y_in, color):
    topRight = Point(x_in, y_in)
    x = topRight.getX()
    y = topRight.getY()
    bottomLeft = Point(x-10, y-10)

    rect = Rectangle(bottomLeft, topRight)
    rect.setFill(color)
    rect.setWidth(4)
    rect.draw(win)


def drawPath(init, end, color, width):
    newInit = checkVar(init)
    newEnd = checkVar(end)

    start = Point(int(newInit[0])-5, int(newInit[1])-5)
    stop = Point(int(newEnd[0])-5, int(newEnd[1])-5)
    path = Line(start, stop)
    path.setWidth(width)
    path.setOutline(color)
    path.draw(win)


def drawTown():
    square(a[0], a[1], 'red')
    square(b[0], b[1], 'yellow')
    square(c[0], c[1], 'red')
    square(d[0], d[1], 'yellow')
    square(e[0], e[1], 'red')
    square(f[0], f[1], 'yellow')
    square(g[0], g[1], 'red')
    square(h[0], h[1], 'yellow')
    square(i[0], i[1], 'red')
    square(l[0], l[1], 'red')
    square(m[0], m[1], 'red')
    square(n[0], n[1], 'red')
    square(o[0], o[1], 'red')
    square(p[0], p[1], 'red')
    square(r[0], r[1], 'red')
    square(s[0], s[1], 'red')
    square(t[0], t[1], 'red')
    square(u[0], u[1], 'red')
    square(v[0], v[1], 'red')
    square(z[0], z[1], 'red')

    
    drawPath('o', 'z', 'red', 2)
    drawPath('o','s','blue',2)
    drawPath('z','a','red',2)
    drawPath('a','t','blue',2)
    drawPath('t','l','red',2)
    drawPath('l','m','red',2)
    drawPath('m','d','yellow',2)
    drawPath('a','s','red',2)
    drawPath('d','c','red',2)
    drawPath('r','c','red',2)
    drawPath('s','r','red',2)
    drawPath('s','f','red',2)
    drawPath('r','p','yellow',2)
    drawPath('f','b','red',2)
    drawPath('b','g','red',2)
    drawPath('b','u','red',2)
    drawPath('u','h','red',2)
    drawPath('h','e','blue',2)
    drawPath('u','v','blue',2)
    drawPath('i','v','red',2)
    drawPath('i','n','red',2)
    drawPath('p','c','red',2)
    drawPath('p','b','red',2)

    win.getMouse()
    win.close()


drawTown()
